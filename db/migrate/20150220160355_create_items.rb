class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :content
			t.datetime :due_date
			t.boolean :completed, default: false
			t.string :category
			t.string :priority
      t.references :list, index: true

      t.timestamps null: false
    end
    add_foreign_key :items, :lists
    add_index :items, [:list_id, :created_at]
  end
end
