# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(name:  "Example User",
             email: "example@example.org",
             password:              "foobar",
             password_confirmation: "foobar",
              activated: true,
              activated_at: Time.zone.now)

20.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@example.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
5.times do
  name = [:Chores, :Work, :Other, :Personal, :Lifestyle, :Vacation, :Wedding].sample
  users.each { |user| user.lists.create!(name: name) }
end

lists = List.order(:created_at).take(6)
20.times do
  content = Faker::Lorem.sentence(6)
  priority = [:low, :medium, :high].sample
  category = [:car, :office, :home, :hobby, :taxes].sample
  lists.each { |list| list.items.create!(content: content, due_date: Time.zone.now, category: category, 
    priority: priority) }
end
