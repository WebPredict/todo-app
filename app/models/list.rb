class List < ActiveRecord::Base
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :name, presence: true
  
  belongs_to :user
  has_many :items, dependent: :destroy
  
end
