class Item < ActiveRecord::Base
  belongs_to :list
  default_scope -> { order(created_at: :desc) }
  validates :list_id, presence: true
  validates :content, presence: true
  
end
