class StaticPagesController < ApplicationController
  def home
    if logged_in?
      if current_user.lists.any?
        @list = current_user.lists.first
      else
        @list = current_user.lists.build
        @list.name = "New List"
        @list.save
      end
      @item = @list.items.build
      if params[:show_all] == 'true'
        @feed_items = current_user.feed.paginate(page: params[:page])
        @num_items_todo = @feed_items.count  
        @num_items_completed = @feed_items.count - current_user.incomplete_feed.count
      else
        @feed_items = current_user.incomplete_feed.paginate(page: params[:page])
        @num_items_todo = current_user.feed.count 
        @num_items_completed = @num_items_todo - @feed_items.count
      end
    end
  end

  def submitcontact
    UserMailer.contact_admin(params[:email], params[:comment]).deliver
    flash[:success] = "Thanks for your feedback... we will review it soon."
    redirect_to root_path
  end
  
  def help
  end
  
  def about
  end

  def contact
  end 
  
end
