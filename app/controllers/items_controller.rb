class ItemsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy
  
  def index
    query = ''
    searchparam = ""
    paramarr = []
    if params[:search] && params[:search] != ''
        query += ' lower(content) LIKE ? '
        searchparam = "%#{params[:search].downcase}%"
    end

    @feed_items = Item.where(query, searchparam).paginate(page: params[:page], per_page: 10)

    @searched = query != ''
  end

  def create
    @list = List.find_by(id: item_params[:list_id])
    @item = @list.items.build(item_params)
    if @item.save
      flash[:success] = "Item created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def update
    @item = Item.find(params[:id])
    respond_to do |format|
      @item.completed = true
      if @item.save
        format.html { redirect_to @item.list.user, notice: 'Item updated.' }
        format.json { render :show, status: :ok, location: @item.list.user }
      end
    end
  end
  
  def destroy
    @item.destroy
    flash[:success] = "Item deleted."
    redirect_to request.referrer || root_url
  end

  private

    def item_params
      params.require(:item).permit(:content, :duedate, :category, :completed, :priority, :list_id)
    end
    
    def current_list
      @item = current_list.items.find_by(id: params[:id])
      redirect_to root_url if @item.nil?
    end
    
end
